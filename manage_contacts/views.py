from django.shortcuts import render, render_to_response, redirect
from django.template.context_processors import csrf

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from manage_contacts.db_work import get_all_user, add_db_user, del_db_user, edit_db_user

@login_required
def index(request):
    info = get_all_user(str(request.user.username))

    return render(request,'index.html',{'contacts':info[0],"last_id":info[1]["max"]})

@login_required
def add_user(request):
    new_user = {
        'name':request.POST.get('name'),
        'company':request.POST.get('company'),
        'email':request.POST.get('email'),
        'phone':request.POST.get('phone'),
        'interest':request.POST.get('interest'),
        'user':request.user.username
    }
    add_db_user(new_user)
    return JsonResponse({'status':'ok'})

@login_required
def del_user(request):
    id = request.POST.get('id')
    print(id)
    del_db_user(id)
    return JsonResponse({'status':'ok'})

@login_required
def edit_user(request):

    upd_us = {
        'id': request.POST.get('id'),
        'name': request.POST.get('name'),
        'company': request.POST.get('company'),
        'email': request.POST.get('email'),
        'phone': request.POST.get('phone'),
        'interest': request.POST.get('interest'),
    }
    edit_db_user(upd_us)
    return JsonResponse({'status':'ok'})
