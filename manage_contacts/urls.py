from django.conf.urls import include, url
from django.contrib import admin
from . import views

from manage_contacts.views import index, add_user,del_user,edit_user

urlpatterns = [
    url(r'^index/$', index, name = 'index'),
    url(r'^adduser/$', add_user, name='add_user'),
    url(r'^deluser/$', del_user, name='del_user'),
    url(r'^edituser/$', edit_user, name='edit_user'),
]
