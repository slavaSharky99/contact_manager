import psycopg2
from psycopg2.extras import RealDictCursor

from contacts.settings import DATABASES
import re

def connect(DATABASES):
    connect = psycopg2.connect(
                dbname= DATABASES['default']['NAME'],
                user= DATABASES['default']['USER'],
                password= DATABASES['default']['PASSWORD'],
                host= DATABASES['default']['HOST'],
                port= DATABASES['default']['PORT']
                )
    connect.autocommit = True
    cursor = connect.cursor(cursor_factory=RealDictCursor)
    return connect, cursor

def get_all_user(user):

    select = """
        SELECT * FROM "cont"
        WHERE major_user = %s
    """
    conn, curs = connect(DATABASES)
    curs.execute(select, (user,))
    i = curs.fetchall()

    last_id = """
        SELECT MAX(id) FROM "cont"
     """
    curs.execute(last_id)
    id_m = curs.fetchone()

    curs.close()
    conn.close()
    return i, id_m

def add_db_user(new_user):

    email_valid = re.findall(r'[\w\.-]+@[\w\.-]+(\.[\w]+)+',new_user['email'])
    for i in email_valid:
        if i == '':
            return False
    insert = """
        INSERT INTO "cont" (name, company, email, phone, interest, major_user)
        VALUES (%(name)s,%(company)s,%(email)s,%(phone)s,%(interest)s,%(user)s)
    """
    conn, curs = connect(DATABASES)
    curs.execute(insert, new_user)
    curs.close()
    conn.close()
    return True

def del_db_user(id):

    delete = """
        DELETE FROM "cont"
        where id = %s
    """

    conn, curs = connect(DATABASES)
    curs.execute(delete,(id,))
    curs.close()
    conn.close()
    return True

def edit_db_user(upd_user):
    update = """
        UPDATE "cont"
        SET name=%(name)s, company=%(company)s, email=%(email)s, phone=%(phone)s,
            interest=%(interest)s
        WHERE id=%(id)s
    """
    conn, curs = connect(DATABASES)
    curs.execute(update, upd_user)
    curs.close()
    conn.close()
    return True
