$(document).ready(function()
{
  // console.log(window.location.href);
  $.ajax({
    url:window.location.href,
    type:'POST',
    data:{
        'csrfmiddlewaretoken': getCookie('csrftoken'),
        'data':'g',
    },
    success:function(response) {
      // console.log(response.x);
      // set the dimensions and margins of the graph
      var data ={};
      data.x = response.x;
      data.y = response.y;
      plot_data(data);
      // console.log(data);
      // example();
    },
  });
});

function plot_data(data_r)
{
  var margin = {top: 5, right: 20, bottom: 30, left: 100},
    width = 1200 - margin.left - margin.right,
    height = 700 - margin.top - margin.bottom;
    // set the ranges
var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);
var svg = d3.select("#d").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
.append("g")
  .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

var getdata = function(data) {
  var dataset = []
  for (var i = 0; i < data.x.length; i++) {
    var x = data.x[i];
    var y = data.y[i];
    dataset.push({"x": x, "y": y});
    }
  return dataset
}

// Get the data
var data = getdata(data_r)
console.log(data);
// format the data
data.forEach(function(d) {
d.x = +d.x;
d.y = +d.y;
});
// scale the range of the data
x.domain([0, 750]);
y.domain([0, 6500]);

// add the dots
svg.selectAll("dot")
.data(data)
.enter().append("circle")
.attr("r", 2)
.attr("cx", function(d) { return x(d.x); })
.attr("cy", function(d) { return y(d.y); })
.style("fill", "rgb(19, 144, 216)");

// add the X Axis
svg.append("g")
.attr("class", "axisWhite")
.attr("transform", "translate(0," + height + ")")
.call(d3.axisBottom(x));

// add the Y Axis
svg.append("g")
.attr("class", "axisWhite")
.call(d3.axisLeft(y));
}
