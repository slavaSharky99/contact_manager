function getCookie(name) {
   var cookieValue = null;
   if (document.cookie && document.cookie !== '') {
       var cookies = document.cookie.split(';');
       for (var i = 0; i < cookies.length; i++) {
           var cookie = jQuery.trim(cookies[i]);
           // Does this cookie string begin with the name we want?
           if (cookie.substring(0, name.length + 1) === (name + '=')) {
               cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
           }
       }
   }
   return cookieValue;
 }
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


$( function() {

  var dialog, form,
    emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im,
    name = $( "#name" ),
    company = $('#company'),
    email = $( "#email" ),
    phone = $( "#phone" ),
    interest = $("#interest"),
    allFields = $( [] ).add( name ).add(company).add( email ).add( phone ).add(interest),
    tips = $( ".validateTips" );

  function updateTips( t ) {
    tips
      .text( t )
      .addClass( "ui-state-highlight" );
    setTimeout(function() {
      tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
  }

  function checkLength( o, n, min, max ) {
    if ( o.val().length > max || o.val().length < min ) {
      o.addClass( "ui-state-error" );
      updateTips( "Length of " + n + " must be between " +
        min + " and " + max + "." );
      return false;
    } else {
      return true;
    }
  }

  function checkRegexp( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
      o.addClass( "ui-state-error" );
      updateTips( n );
      return false;
    } else {
      return true;
    }
  }

  function addUser() {
    var valid = true;
    allFields.removeClass( "ui-state-error" );

    valid = valid && checkLength( name, "username", 3, 16 );
    valid = valid && checkLength( email, "email", 6, 80 );
    valid = valid && checkLength( phone, "phone", 6, 11 );

    valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
    valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
    valid = valid && checkRegexp( phone, phoneRegex, "Phone field only allow : 0-9" );
    console.log(last_id, "LAST");
    var buttons = '<div class=" btn-group" role="group" >' +
                  '<button type="button" class="btn_color btn btn-default" onclick="show_contact("'+last_id+'")>Details</button>' +
                  '<button type="button" class="btn_color btn btn-default" onclick="edit_contact("'+last_id+'")>Edit</button>' +
                  '<button type="button" class="btn_color btn btn-default" onclick="delete_contact("'+last_id+'")>Delete</button>' +
                  '</div>';
    if ( valid ) {
      $( "#users tbody" ).append( "<tr>" +
        "<td>" + name.val() + "</td>" +
        "<td>" + company.val() + "</td>" +
        "<td>" + email.val() + "</td>" +
        "<td>" + phone.val() + "</td>" +
        "<td>" + interest.val() + "</td>" +
        "<td>" + buttons + "</td>" +
      "</tr>" );
      $.ajax({
        url:'/manage/adduser/',
        type:'POST',
        dataType: 'json',
        data: {
                'csrfmiddlewaretoken': getCookie('csrftoken'),
                'name':name.val(),
                'company':company.val(),
                'email':email.val(),
                'phone':phone.val(),
                'interest':interest.val()
              },
        success: function (data) {
          console.log(data);
        },
        error: function (request, status, error) {
          alert(request.responseText);
        }
      });
      dialog.dialog( "close" );

    }
    return valid;
  }

  dialog = $( "#dialog-form" ).dialog({
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    buttons: {
      "Create": addUser,
      Cancel: function() {
        dialog.dialog( "close" );
      }
    },
    close: function() {
      form[ 0 ].reset();
      allFields.removeClass( "ui-state-error" );
    }
  });



  form = dialog.find( "form" ).on( "submit", function( event ) {
    event.preventDefault();
    addUser();
  });

  $( "#create-user" ).button().on( "click", function() {
    dialog.dialog( "open" );
  });

} );
