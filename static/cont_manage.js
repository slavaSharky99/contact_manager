var status_details = 0;
var id_ = 0;
var _id_;
//loading popup with jQuery magic!
function load_details(){
	//loads popup only if it is disabled
	if(status_details==0){
		$("#background").css({
			"opacity": "0.7"
		});
		$("#background").fadeIn("slow");
		$("#show_details").fadeIn("slow");
		status_details = 1;
	}
}

//disabling popup with jQuery magic!
function disable_ditails(){
	//disables popup only if it is enabled
	if(status_details==1){
		$("#background").fadeOut("slow");
		$("#show_details").fadeOut("slow");
		status_details = 0;
	}
}

//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#show_details").height();
	var popupWidth = $("#show_details").width();
	//centering
	$("#show_details").css({
    "font-size":"20px",
    "background-color":"rgb(0,0,0)",
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});

}

//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){

	//LOADING POPUP
	//Click the button event!
	$(".btn_details").click(function(){
		centerPopup();
		//load popup
		load_details();
	});
  $(".btn_del").click(function(){
		centerPopup();
		//load popup
		load_details();
	});
	$(".btn_edit").click(function(){
		centerPopup();
		$("#show_details").css({
			"font-size":"12px",
			"height":"65%"
		});
		load_details();
	});
	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose").click(function(){
		disable_ditails();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disable_ditails();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && status_details==1){
			disable_ditails();
		}
	});

});
function show_contact(id)
{
  $("#show_details").html('');
  info = data[id];
  $('<div> Запись </div>'+
    '<ul>'+
    '<li>'+"ID: "+info["id"]+'</li>'+
    '<li>'+"Name: "+info["name"]+'</li>'+
    '<li>'+"Company: "+info["company"]+'</li>'+
    '<li>'+"Email: "+info["email"]+'</li>'+
    '<li>'+"Phone: "+info["phone"]+'</li>'+
    '<li>'+"Interest: "+info["interest"]+'</li>'+
    '</ul>'
  ).appendTo("#show_details");

}
function delete_contact(id)
{
  // console.log(id);
	id_ = id
  $("#show_details").html('');
  $('<div> Запись </div>'+
    '<div> Вы действительно хотите удалить контакт? </div>'+
    '<button class="btn_color btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
    'Cancel' + '</button>'+
    '<button class="btn_color btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="del()">'+
    'Delete' + '</button>'
  ).appendTo("#show_details");
	disable_ditails();
}
function del()
{
	$.ajax({
		url:'/manage/deluser/',
		type:'POST',
		dataType: 'json',
		data: {
						'csrfmiddlewaretoken': getCookie('csrftoken'),
						'id':id_,
					},
		success: function(data){
			console.log(data);
			$("#"+id_+"_tr").html('');
			disable_ditails();
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function edit_contact(id)
{
	$("#show_details").html('');
	info = data[id];
	_id_ = info["id"];
	$('<div> Edit Contact </div>'+
	 	'<div id="dialog-form" title="Create new user">'+
		'<form>'+
			'<fieldset>'+
			'<label for="name">Name</label>'+
			'<input type="text" name="name" id="name_" value="'+info["name"]+'" class="text ui-widget-content ui-corner-all">'+
			'<label for="name">Company</label>'+
			'<input type="text" name="company" id="company_" value="'+info["company"]+'" class="text ui-widget-content ui-corner-all">'+
			'<label for="email">Email</label>'+
			'<input type="text" name="email" id="email_" value="'+info["email"]+'" class="text ui-widget-content ui-corner-all">'+
			'<label for="password">Phone</label>'+
			'<input type="text" name="phone" id="phone_" value="'+info["phone"]+'"  class="text ui-widget-content ui-corner-all">'+
			'<label for="name">Interest</label>'+
			'<input type="text" name="interest" id="interest_" value="'+info["interest"]+'" class="text ui-widget-content ui-corner-all">'+
			'<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">'+
		'</fieldset>'+
		'<div class=" btn-group" role="group" >'+
				'<button type="button" class="btn_color btn btn-default " >Cancel</button>'+
				'<button type="button" class="btn_color btn btn-default " onclick="ed_cont()">Edit</button>'+
				'</div>'+
	'</form>'+
'</div>'
	).appendTo("#show_details");

}
function ed_cont() {
	var name = $("#name_").val();
	var comp = $("#company_").val();
	var mail = $("#email_").val();
	var phone = $("#phone_").val();
	var inter = $("#interest_").val();

	$.ajax({
		url:'/manage/edituser/',
		type:'POST',
		dataType: 'json',
		data: {
						'csrfmiddlewaretoken': getCookie('csrftoken'),
						'id':_id_,
						'name':name,
						'company':comp,
						'email':mail,
						'phone':phone,
						'interest':inter
					},
		success:function(response) {
			console.log(response);
		},
		error:function(error) {
			console.log(error);
		}
	});
}
