from django.conf.urls import include, url
from django.contrib import admin

from loginsys.views import login
from loginsys.views import logout
from loginsys.views import registration

urlpatterns = [
    url(r'^login/$', login),
    url(r'^logout/$', logout ),
    url(r'^registration/$', registration),
]
