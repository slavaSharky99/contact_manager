from django import forms

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegForm(UserCreationForm):

    def __init__(self, *args, **kwargs):

       super(RegForm, self).__init__(*args, **kwargs)
       # del self.fields['password1']
       del self.fields['password2']


    class Meta:

        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password1'
        ]
        widgets = {
            'username': forms.TextInput(
                    attrs = {'placeholder': 'Login', 'name':'username'} ),
            'first_name': forms.TextInput(
                    attrs = {'placeholder': 'First Name', 'name':'first_name'} ),

            'last_name': forms.TextInput(
                    attrs = {'placeholder': 'Last Name', 'name':'last_name'} ),
            'email': forms.TextInput(
                    attrs = {'placeholder': 'Email', 'name':'email'} ),
            'password1': forms.TextInput(
                    attrs = {'placeholder': 'Пароль', 'name':'password1', 'type':'password'} )
        }
        labels = {
            'username': '',
            'first_name': '',
            'last_name': '',
            'email': '',
            'password1': None,
        }
        help_texts = {
                'username': '',
                'first_name': '',
                'password1': '',
        }

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
